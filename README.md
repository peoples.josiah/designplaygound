# playground

playground
This repo is designed to work with `NPM`. If you would like to use another package manager, you may need to adjust your code a little bit to support it.

## Underlying Technology

- [TypeScript](https://www.typescriptlang.org/): JavaScript with syntax for types
- [React](https://reactjs.org/): JavaScript library for building user interfaces
- [Next.js](https://nextjs.org/): React Framework for Production
- [tailwindcss](https://tailwindcss.com/): Utility-first CSS framework packed with classes that can be composed to build any design, directly in your markup
- [Storybook](https://storybook.js.org/): Frontend workshop for building UI components and pages in isolation
- [Jest](https://jestjs.io/): JavaScript Testing Framework with a focus on simplicity
- [eslint](https://eslint.org/): Pluggable linting utility for JavaScript and JSX
- [Felt](https://gitlab.com/heb-engineering/teams/spm-eng/ilm/ilm-bobcat/ilm-ui/felt): The shared frontend resource for StoreXP Infrastructure
- [Mortar](https://mortar.heb.com): The HEB design system


## File Organization Structure

- [`.storybook/`](./.storybook/): Storybook configuration
- [`public/`](./public/): Static assets available to UI
- [`src/`](./src/): Application code
    - [`components/`](./src/components/): Embeddable components used by the app.
    - [`pages/`](./src/pages/): Next.js routes.
    - [`styles/`](./src/styles/): Global application CSS.

## Getting Started

### Setting up your environment:
```sh
cp .env.example .env
```


## Dev Container Setup
This will take care of setting up your development environment and installing all requirements & dependencies. You can start coding 
immediately. Get started using `npm run dev`! For more information see the [documentation](https://code.visualstudio.com/docs/devcontainers/containers).

### VS Code
To use Dev Containers in VSCode you must have the [Dev Container Extension](vscode:extension/ms-vscode-remote.remote-containers)
installed. Then when you open the project you should see a prompt in the bottom right to open the project in a Dev Container. 
If you do not see the prompt. Hit `F1` and choose `Rebuild and Reopen in container`.

### Jetbrains
To use Dev Containers in a Jetbrains IDE you must manually go to the `.devcontainer/devcontainer.json` file. 
In the top left of the file you will see a docker icon that you can click to launch the Dev Container configuration.

### Running the Service in Development Mode
If you do not want to use Dev Containers at the IDE level, you can still develop inside a container using:
```sh
make dev
```

Once running, you should be able to acccess the service at `http://localhost:3000`
    
### Running the Service in Production Mode

Alternatively you can build a production ready container to run the service like it would on kubernetes:
```sh
make prod
```

Once running, you should be able to acccess the service at `http://localhost:3000`

- You must make sure the `dev` container is stopped first, since they run on the same port.
- This will build during container creation but won't automatically rebuild when the code updates. You will need to stop and restart the container.
- Generally speaking, because of the build complexity here, you'll probably only ever need to run this if you're trying to mimic a kubernetes environment, and not for local development.

### Installing or Updating Node Dependencies Inside of Running Containers
Since `npm install` is ran during the container creation, unless you blow away your containers and rebuild from scratch, any dependency changes made in your `package.json` file won't automatically be reflected inside the respective containers.

You can force a reinstall in the dev container with:

```sh
make install
```

### Running Tests

While the `dev` container is running:

```sh
make test
```

### Running the Linter

While the `dev` container is running:

```sh
make lint
```

You can attempt autofix errors with:
```sh
make lint-fix
```

### Cleaning Old Containers

#### Stop and remove the dev container
```sh
make clean-dev
```

#### Stop and remove the production container
```sh
make clean-prod
```

#### Stop and remove all images and volumes
```sh
make clean
```



## Developer Guides

- [String Translation](docs/string-translation.md)
