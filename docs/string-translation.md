# String Translation

Strings translations can load both in pages and sub-components within a Next.js application, but implementation details are a little bit different depending on context.

## Concepts

- `language`/`locale`/`lng`:  The language used for a translation. 
- `fallbackLng`: The language to fall back onto if a translation cannot be found (this will always be `en`)
- `namespace`/`ns`: A group of translation strings that are organized together based on common feature or functionality
  - Any string that gets shared in multiple contexts belong in the `common` namespace. 
  - Namespaces are declared in `public/locales/{lng}/{ns}.json` files.
  - Namespaces should not include `-` or `/` (i.e., no subdirectories) as `i18next` seems to have issues loading these. We're opting to use camel case for namespaces at the present (e.g., `pageTemplate`).
  - Namespace files can be multiple levels deep. Use a `.` when referencing a subitem. For example, `t('section.barcode.tabName')` would map to `Barcode` in this namespace:
      ```json
      {
        "section": {
          "barcode": {
            "tabName": "Barcode"
          }
        }
      }
      ```
  - When using multiple namespaces, strings located in the default namespace (i.e., the first in the list) can be referenced with no namespace. All other strings require a namespace declaration. This is done to by `i18next` to avoid variable collisions. For example, if your default namespace is `pageTemplate`:
    - `t('pageTitle')` will load the `pageTitle` variable out of the `pageTemplate` namespace.
    - `t('common:appName')` will load the `appName` variable out of the `common` namespace. 
    
      You cannot call `t('appName')` directly.


## Translations

### Server-Side Page Translation

Server-side translations get loaded into page-level components, either via [`getStaticProps()`]https://nextjs.org/docs/pages/building-your-application/data-fetching/get-static-props) or [`getServerSideProps()`](https://nextjs.org/docs/pages/building-your-application/data-fetching/get-server-side-props) depending on how you want to inject props for a particular page.

For example:

```typescript
import { useTranslation } from 'next-i18next';
import { getServerSideTranslations } from '../../util/i18n/i18n';

// Translation namespaces to load for this particular page.
const translationNamespaces = ['pageTemplate', 'otherNamespace'];

export const getServerSideProps = async ({locale}: GetServerSidePropsContext) => {
  const translationProps = await getServerSideTranslations(translationNamespaces, locale);

  return {
    props: {
      ...translationProps,
    },
  }
}

export default function Template() {
  const { t } = useTranslation(translationNamespaces);

  return <div>
    I can now call { t('pageName') }, because it is in the `pageTemplate` namespace, or {t('otherNamespace:section.barcode.tabName')} and {t('common:appName')} from their respective namespaces.
  </div>
}
```

Notice that you do not have to explicitly add `common` to the namespace list as `getServerSideTranslations()` will already do that behind the scenes.

### Sub-Component Translation

Any sub components will need the corresponding namespace declared in the page component. This is important or else the component won't inherit the translations. For example, the `/` route uses the `template-list` component, so you can do something like this:

In `pages/index.page.tsx`:

```typescript

const translationNamespaces = ['pageHome', 'componentTemplateList'];

export async function getStaticProps({locale}: GetStaticPropsContext) {
  const translationProps = await getServerSideTranslations(translationNamespaces, locale);
  return {
    props: {
      ...translationProps,
      templates: await loadTemplateList(),
    },
    revalidate: 10,
  }
}
```

In `components/template-list.tsx`:

```typescript
import { useTranslation } from 'next-i18next';

export default function TemplateList(props: TemplateListProps) {
  const { t } = useTranslation('componentTemplateList');

  return <div>
    I can now call {t('availableTemplates')} which is inside the `componentTemplateList` namespace, or {t('pageHome:pageTitle')} and {t('common:appName')} from the other namespaces.
  </div>
}
```

In `components/template-editor.tsx`:
```typescript
import { useTranslation } from "next-i18next";

export default function TemplateEditor(props: TemplateEditorProps) {
  const { i18n, t } = useTranslation();

  return <div>{t('common:loading')}</div>;
}
```


### Storybook Translation

Translations in Storybook don't automatically have context of the location of namespaces. This could potentially be solved in the future by a custom webpack plugin in `.storybook/main.ts` that reads namespaces from the file system, but this isn't a priority at the moment, so namespaces currently have to be explicitly added to the `ns` settings in `.storybook/i18n.ts`:

```typescript
i18n
  .use(Backend)
  .use(initReactI18next)
  .init({
    fallbackLng: 'en',
    lng: 'en',
    debug: true,
    backend: {
      loadPath: "/locales//.json"
    },
    ns: [
      'common',
      'pageHome',
      'pagePreview',
      'pageTemplate',
      'componentTemplateList'
    ]
  }
);
```

### TypeScript Considerations

In some cases, `t()` returns a `null` value. In many cases this is okay. However, in some cases, e.g., Passing data into Mortar props, `null` isn't a valid option. So you need to pass the expected type (i.e. `string`). For example:

```typescript
  <Input
    label={ t<string>('section.templateInfo.widthLabel') }
    hint={ t<string>('section.templateInfo.dimensionHint') }
    type="text"
    />
```
