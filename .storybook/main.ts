import type { StorybookConfig } from "@storybook/nextjs";
import pkg from '../package.json';

const config: StorybookConfig = {
  stories: [
    {
      directory: "../src/pages",
      titlePrefix: `${pkg.name}/Pages`,
      files: "**/*.stories.@(js|jsx|ts|tsx)"
    },
    {
      directory: "../src/pages",
      titlePrefix: `${pkg.name}/Pages`,
      files: "**/*.mdx"
    },
    {
      directory: "../src/components",
      titlePrefix: `${pkg.name}/Components`,
      files: "**/*.stories.@(js|jsx|ts|tsx)"
    },
    {
      directory: "../src/components",
      titlePrefix: `${pkg.name}/Components`,
      files: "**/*.mdx"
    },
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    "storybook-react-i18next",
  ],
  framework: {
    name: "@storybook/nextjs",
    options: {},
  },
  docs: {
    autodocs: "tag",
  },
  webpackFinal: (config: any) => {
    config.resolve.fallback.fs = false;
    return config;
  },
};

export default config;

