import type { Preview } from "@storybook/react";
import i18n from './i18next';

const preview: Preview = {
  globals: {
    locale: 'en',
    locales: {
      en: { title: 'English', left: '🇺🇸' },
      es: { title: 'Spanish', left: '🇲🇽' },
    },
  },
  parameters: {
    backgrounds: {
      default: "light",
    },
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
    i18n,
  },
};

export default preview;

