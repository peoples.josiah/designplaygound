FROM node:lts-alpine AS base

FROM base as dev
WORKDIR /app
ENV NODE_EXTRA_CA_CERTS=/app/certs/HEB.crt
RUN apk add git

# Install dependencies only when needed
FROM base AS deps
RUN apk add --no-cache libc6-compat
WORKDIR /app

# Copy needed files
COPY package.json package-lock.json* ./
COPY .npmrc ./
COPY certs/HEB.crt certs/HEB.crt

# Install the certificates
ENV NODE_EXTRA_CA_CERTS=/app/certs/HEB.crt

# Install the package dependencies
RUN npm install


# Production image, copy all the files and run next
FROM base AS runner

RUN apk add --no-cache curl

ENV NODE_ENV production
ENV NEXT_TELEMETRY_DISABLED 1

WORKDIR /app

COPY --from=deps /app/node_modules ./node_modules
COPY . .

EXPOSE 3000

ENV PORT 3000

RUN addgroup --gid 1001 -S appuser && adduser -S -G appuser --uid 1001 appuser \
  && chown -R appuser:appuser /app
USER 1001:1001

RUN npm run build

HEALTHCHECK --interval=5s --timeout=5s --start-period=10s \
  CMD curl --fail http://localhost:3000 || exit 1

CMD ["npm", "start"]
