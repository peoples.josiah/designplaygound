import JSDOMEnvironment from "jest-environment-jsdom";

/**
 * Resolves an issue with jest/jsdom where structuredClone is undefined.
 *
 * @see https://github.com/jsdom/jsdom/issues/3363
 */
export default class FixJSDOMEnvironment extends JSDOMEnvironment {
  constructor(...args: ConstructorParameters<typeof JSDOMEnvironment>) {
    super(...args);

    this.global.structuredClone = structuredClone;
  }
}

