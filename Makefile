# Cleanup containers
clean:
	docker compose down --remove-orphans --rmi all --volumes

clean-dev:
	docker compose down --remove-orphans --rmi development

clean-prod:
	docker stop playground

# Dev Mode
develop:
	docker compose up dev

dev: develop

# Production Mode
production:
	docker build . -t playground:latest && docker run --name playground -p 3000:3000 --rm --detach playground

prod: production

# Install/update dependencies inside dev container
install:
	docker compose run dev npm install

# Run Linter
lint:
	docker compose run dev npm run lint

lint-fix:
	docker compose run dev npm run lint:fix

# Run Tests
test:
	docker compose run dev npm run test:ci

# Watch Tests
test-watch:
	docker compose run dev npm run test:watch

# Initialize Storybook
storybook:
	docker compose run dev npm run dev:storybook

