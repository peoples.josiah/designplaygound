import Home from "./index.page";

export default {
  title: "Pages/Home",
  component: Home,
};

export const HomePage = () => <Home />;
