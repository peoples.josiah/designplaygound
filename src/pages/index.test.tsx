import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";

import Home from "./index.page";

describe("Home", () => {
  it("renders a heading", () => {
    render(<Home />);

    const heading = screen.getByRole("heading");

    expect(heading).toBeInTheDocument();
  });
});
