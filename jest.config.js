const nextJest = require('next/jest');

const createJestConfig = nextJest({
  dir: './',
});

const customJestConfig = {
  setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],
  testEnvironment: "./jest.jsdom.ts",
  testPathIgnorePatterns: [
    '<rootDir>/.next/',
    '<rootDir>/node_modules/',
    '<rootDir>/.devops',
  ],
};

/**
 * Hacky jest wrapper since next/jest doesn't allow you to override transformIgnorePatterns,
 * but rather just appends to it. This hack allows us to explicitly exclude the nanoid
 * package from the transformations as its the module that is causing tests to not work.
 *
 * @see https://github.com/vercel/next.js/issues/35634
 */
async function jestConfig() {
  const nextJestConfig = await createJestConfig(customJestConfig)()
  // /node_modules/ is the first pattern so this is where we exclude modules.
  nextJestConfig.transformIgnorePatterns[0] = 'node_modules/?!(lit.*\.*js|.*\.mjs)$';
  return nextJestConfig;
}

module.exports = jestConfig;
